package com.GroupChat.fyf.database;

import android.provider.BaseColumns;

/**
 * Created by Stephen on 6/17/2015.
 */
public class FyfContract
{
    public static final class UserEntry implements BaseColumns
    {
        public static final String TABLE_NAME = "users";
        public static final String COLUMN_EMAIL = "email";
        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_LATITUDE = "latitude";
        public static final String COLUMN_LONGITUDE = "longitude";
    }

    public static final class GroupEntry implements BaseColumns
    {
        public static final String TABLE_NAME = "groups";
        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_IS_PRIVATE = "is_private";
        public static final String COLUMN_END_DATE = "end_date";
    }

    public static final class UsersGroupsEntry implements BaseColumns
    {
        public static final String TABLE_NAME = "users_groups";
        public static final String COLUMN_USER_ID = "user_id";
        public static final String COLUMN_GROUP_ID = "group_id";
    }
}
