package com.GroupChat.fyf.views;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.GroupChat.fyf.R;
import com.GroupChat.fyf.adapters.MessageAdapter;
import com.GroupChat.fyf.adapters.UserAdapter;
import com.GroupChat.fyf.controllers.GroupsController;
import com.GroupChat.fyf.controllers.UsersController;
import com.GroupChat.fyf.model.Group;
import com.GroupChat.fyf.model.Message;
import com.GroupChat.fyf.model.User;
import com.GroupChat.fyf.utilities.BusProvider;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link UserGroupAdd#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UserGroupAdd extends Fragment
{
    private static final String GROUP_ID = "groupId";
    private final String LOG_TAG = getClass().getSimpleName();

    private Group mGroup;
    private boolean mInit;
    private ArrayList<User> searchResults = new ArrayList<User>();

    private ArrayAdapter<User> mListAdapter;
    private ListView mListView;
    private Button mSearchButton;
    private EditText mSearchText;

    public static UserGroupAdd newInstance(Group group)
    {
        UserGroupAdd fragment = new UserGroupAdd();
        Bundle args = new Bundle();
        args.putLong(GROUP_ID, group.getId());
        fragment.setArguments(args);
        return fragment;
    }

    public UserGroupAdd()
    {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
        {
            GroupsController.getInstance(getActivity()).getGroup(getArguments().getLong(GROUP_ID));
        }
        BusProvider.getEventBus().register(this);
    }

    @Override
    public void onDestroy()
    {
        BusProvider.getEventBus().unregister(this);
        super.onDestroy();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        View parent = inflater.inflate(R.layout.fragment_user_group_add, container, false);

        mListView = (ListView) parent.findViewById(R.id.lv_user_search);
        mSearchButton = (Button) parent.findViewById(R.id.btn_search);
        mSearchText = (EditText) parent.findViewById(R.id.et_user_search);

        mSearchButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                search();
            }
        });

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                User user = (User) parent.getItemAtPosition(position);
                if(mGroup != null)
                    UsersController.getInstance(getActivity()).addUserToGroup(String.valueOf(mGroup.getId()), String.valueOf(user.getId()));
            }
        });

        return parent;
    }

    public void search()
    {
        UsersController.getInstance(getActivity()).searchUsers(mSearchText.getText().toString());
    }

    @Subscribe
    public void getMessage(Group group)
    {
        mGroup = group;
    }

    @Subscribe
    public void getMessage(ArrayList<User> users)
    {
        this.searchResults = users;
        mListAdapter = new UserAdapter(getActivity(), users);
        mListView.setAdapter(mListAdapter);
    }

    @Subscribe
    public void getMessage(User user)
    {
        if(user != null)
        {
            getActivity().getSupportFragmentManager().popBackStack();
        }
    }
}
