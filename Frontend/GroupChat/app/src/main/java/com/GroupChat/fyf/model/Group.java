package com.GroupChat.fyf.model;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by Stephen on 6/30/2015.
 */
public class Group
{
    private String name;
    private long id;
    private ArrayList<Message> messages = new ArrayList<>();
    private ArrayList<User> users = new ArrayList<>();

    public Group(long id, String name)
    {
        this.id = id;
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public ArrayList<Message> getMessages()
    {
        return this.messages;
    }

    public void setMessages(ArrayList<Message> messages)
    {
        this.messages = messages;
    }

    public ArrayList<User> getUsers() { return this.users; }

    public void setUsers(ArrayList<User> users) { this.users = users; }
}
