package com.GroupChat.fyf.controllers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Log;

import com.GroupChat.fyf.model.Message;
import com.GroupChat.fyf.model.User;
import com.GroupChat.fyf.utilities.PreferenceUtility;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.GroupChat.fyf.database.FyfContract;
import com.GroupChat.fyf.database.FyfDbHelper;
import com.GroupChat.fyf.model.Group;
import com.GroupChat.fyf.utilities.BusProvider;
import com.GroupChat.fyf.utilities.NetworkRequestSingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Created by Stephen on 7/28/2015.
 */
public class GroupsController
{
    private Hashtable<Long, Group> mGroups = new Hashtable<Long, Group>();
    private static GroupsController mController;
    private static Context mContext;
    private static FyfDbHelper mFyfDbHelper;
    private static String LOG_TAG = "GroupsController";
    private long tempGroupId;

    private GroupsController(Context context)
    {
        mContext = context;
        mFyfDbHelper = new FyfDbHelper(context);
    }

    public static synchronized GroupsController getInstance(Context context)
    {
        if(mController == null)
        {
            mController = new GroupsController(context);
        }
        return mController;
    }

    public void createGroup(String name)
    {
        String requestURL = Uri.parse(NetworkRequestSingleton.ServerURL + "/groups")
                .buildUpon()
                .appendQueryParameter("name", name)
                .build().toString();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.POST, requestURL, (String)null,
                new Response.Listener<JSONObject>() {
                    Group group = null;
                    @Override
                    public void onResponse(JSONObject response) {
                        long id = -1;
                        String name = null;
                        try {
                            id = response.getLong("id");
                            name = response.getString("name");
                        }
                        catch(JSONException e)
                        {
                            e.printStackTrace();
                            Log.e(LOG_TAG, "Json Object doesn't match");
                            return;
                        }
                        UsersController.getInstance(mContext).addUserToGroup(String.valueOf(id), PreferenceUtility.getLoggedInID(mContext));
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO Auto-generated method stub

            }
        });
        NetworkRequestSingleton.getInstance(mContext).addToRequestQueue(jsonObjectRequest);
    }

    public Group getGroup(long id)
    {
        //attempt to retrieve from hashtable
//        Group group = mGroups.get(id);
//        if(group != null)
//        {
//            return group;
//        }
//
//        //attempt to retrieve from local db
//        SQLiteDatabase db = mFyfDbHelper.getReadableDatabase();
//
//        String[] projection = {FyfContract.GroupEntry._ID, FyfContract.GroupEntry.COLUMN_NAME, FyfContract.GroupEntry.COLUMN_IS_PRIVATE, FyfContract.GroupEntry.COLUMN_END_DATE};
//        String selection = FyfContract.GroupEntry._ID + " = ?";
//        String[] selectionArgs = {String.valueOf(id)};
//
//        Cursor c = db.query(FyfContract.GroupEntry.TABLE_NAME,
//                projection,
//                selection,
//                selectionArgs,
//                null,
//                null,
//                null);
//
//        if(c.moveToFirst())
//        {
//            String name = c.getString(c.getColumnIndexOrThrow(FyfContract.GroupEntry.COLUMN_NAME));
//            int isPrivate = c.getInt(c.getColumnIndexOrThrow(FyfContract.GroupEntry.COLUMN_IS_PRIVATE));
//            long endDate = c.getLong(c.getColumnIndexOrThrow(FyfContract.GroupEntry.COLUMN_END_DATE));
//            boolean boolIsPrivate = (isPrivate == 1) ? true : false;
//            group = new Group(id, name, boolIsPrivate, endDate);
//        }
//        c.close();
//        db.close();
//        if(group != null)
//        {
//            mGroups.put(group.getId(), group);
//            return group;
//        }

        //finally, retrieve from external server
        String requestURL = Uri.parse(NetworkRequestSingleton.ServerURL + "/groups/" + id)
                .buildUpon()
                .build().toString();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET, requestURL, (String)null,
                new Response.Listener<JSONObject>() {
                    Group group = null;
                    @Override
                    public void onResponse(JSONObject response) {
                        long id = -1;
                        String name = null;
                        ArrayList<User> userArray = new ArrayList<>();
                        try {
                            id = response.getLong("id");
                            name = response.getString("name");
                            JSONArray users = response.getJSONArray("Users");
                            for(int i = 0; i < users.length(); i++)
                            {
                                JSONObject userObject = users.getJSONObject(i);
                                User user = new User(userObject.getLong("id"), userObject.getString("email"), userObject.getString("name"));
                                userArray.add(user);
                            }
                        }
                        catch(JSONException e)
                        {
                            e.printStackTrace();
                            Log.e(LOG_TAG, "Json Object doesn't match");
                            return;
                        }
                        tempGroupId = id;
                        if(id > -1)
                        {
                            group = new Group(id, name);
                            group.setUsers(userArray);
                        }
                        BusProvider.getEventBus().post(group);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO Auto-generated method stub

            }
        });
        NetworkRequestSingleton.getInstance(mContext).addToRequestQueue(jsonObjectRequest);

        return mGroups.get(id);
    }

    public void updateGroup(long id, String name)
    {
        String requestURL = Uri.parse(NetworkRequestSingleton.ServerURL + "/groups/" + id)
                .buildUpon()
                .appendQueryParameter("name", name)
                .build().toString();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.PUT, requestURL, (String)null,
                new Response.Listener<JSONObject>() {
                    Group group = null;
                    @Override
                    public void onResponse(JSONObject response) {
                        long id = -1;
                        String name = null;
                        try {
                            id = response.getLong("id");
                            name = response.getString("name");
                        }
                        catch(JSONException e)
                        {
                            e.printStackTrace();
                            return;
                        }
                        group = new Group(id, name);
                        BusProvider.getEventBus().post(group);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO Auto-generated method stub
            }
        });
        NetworkRequestSingleton.getInstance(mContext).addToRequestQueue(jsonObjectRequest);
    }

    public void deleteGroup(String id)
    {
        String requestURL = Uri.parse(NetworkRequestSingleton.ServerURL + "/groups")
                .buildUpon()
                .appendPath(id)
                .build().toString();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.DELETE, requestURL, (String)null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Group group = new Group(-1, "deleted");
                        BusProvider.getEventBus().post(group);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO Auto-generated method stub
                Log.d(LOG_TAG, "Error in request: ");
                error.printStackTrace();
            }
        });
        NetworkRequestSingleton.getInstance(mContext).addToRequestQueue(jsonObjectRequest);
    }

    public void getGroupMessages(String userId, Group group) //TODO: REMOVE USER?
    {
        String requestURL = Uri.parse(NetworkRequestSingleton.ServerURL)
                .buildUpon()
                .appendPath("users")
                .appendPath(userId)
                .appendPath("groups")
                .appendPath(String.valueOf(group.getId()))
                .appendPath("messages")
                .build().toString();
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(
                Request.Method.GET, requestURL, (String)null,
                new Response.Listener<JSONArray>() {
                    Group group = null;
                    @Override
                    public void onResponse(JSONArray response) {
                        long id = -1;
                        String name = null;
                        ArrayList<Message> messages = new ArrayList<>();
                        User sender;
                        Group group;
                        try {
                            for(int i = 0; i < response.length(); i++)
                            {
                                JSONObject messageInstance = (JSONObject) response.get(i);
                                JSONObject userObject = messageInstance.getJSONObject("User");
                                JSONObject groupObject = messageInstance.getJSONObject("Group");
                                sender = new User(userObject.getLong("id"), userObject.getString("email"), userObject.getString("name"));
                                group = new Group(groupObject.getLong("id"), groupObject.getString("name"));
                                messages.add(new Message(messageInstance.getLong("id"), messageInstance.getString("text"), sender, group));
                            }
                        }
                        catch(JSONException e)
                        {
                            e.printStackTrace();
                            Log.e(LOG_TAG, "Json Object doesn't match");
                            return;
                        }
                        BusProvider.getEventBus().post(messages);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(LOG_TAG, "Failed Loading Messages");
                // TODO Auto-generated method stub

            }
        });
        NetworkRequestSingleton.getInstance(mContext).addToRequestQueue(jsonArrayRequest);
    }

    public void sendMessage(String userId, Group group, String text)
    {
        String requestURL = Uri.parse(NetworkRequestSingleton.ServerURL)
                .buildUpon()
                .appendPath("users")
                .appendPath(userId)
                .appendPath("groups")
                .appendPath(String.valueOf(group.getId()))
                .appendPath("messages")
                .appendQueryParameter("text", text)
                .build().toString();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.POST, requestURL, (String)null,
                new Response.Listener<JSONObject>() {
                    Group group = null;
                    @Override
                    public void onResponse(JSONObject response) {
                        long id = -1;
                        String text = null;
                        Message message;
                        User user;
                        try {
                            id = response.getLong("id");
                            text = response.getString("text");
                            JSONObject userObject = response.getJSONObject("User");
                            user = new User(userObject.getLong("id"), userObject.getString("email"), userObject.getString("name"));
                            message = new Message(id, text, user);
                        }
                        catch(JSONException e)
                        {
                            e.printStackTrace();
                            Log.e(LOG_TAG, "Json Object doesn't match");
                            return;
                        }
                        BusProvider.getEventBus().post(message);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO Auto-generated method stub

            }
        });
        NetworkRequestSingleton.getInstance(mContext).addToRequestQueue(jsonObjectRequest);
    }
}
