package com.GroupChat.fyf.views;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.GroupChat.fyf.R;
import com.GroupChat.fyf.controllers.GroupsController;
import com.GroupChat.fyf.model.Group;
import com.GroupChat.fyf.model.User;
import com.GroupChat.fyf.utilities.BusProvider;
import com.GroupChat.fyf.utilities.UIUtility;
import com.squareup.otto.Subscribe;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class GroupCreateFragment extends Fragment
{
    private EditText mGroupName;
    private TextView mGroupEndDate;
    private CheckBox mIsPrivate;
    private Button mCreate;

    private final String LOG_TAG = getClass().getSimpleName();
    private Calendar mEndDateCalendar;

    // TODO: Rename and change types and number of parameters
    public static GroupCreateFragment newInstance()
    {
        GroupCreateFragment fragment = new GroupCreateFragment();
        return fragment;
    }

    public GroupCreateFragment()
    {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        BusProvider.getEventBus().register(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_group_create, container, false);

        mGroupName = (EditText) rootView.findViewById(R.id.group_name);
        mCreate = (Button) rootView.findViewById(R.id.btn_group_create);

        mCreate.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String name = mGroupName.getText().toString();
                GroupsController.getInstance(getActivity()).createGroup(name);
            }
        });

        return rootView;
    }

    @Override
    public void onDestroy()
    {
        BusProvider.getEventBus().unregister(this);
        super.onDestroy();
    }

    @Subscribe
    public void getMessage(User user)
    {
        if(user != null)
        {
            Toast.makeText(getActivity(), "Group created successfully!", Toast.LENGTH_SHORT);
            getActivity().getSupportFragmentManager().popBackStack();
        }
        else
        {
            Toast.makeText(getActivity(), "Something went wrong! Group was not created!", Toast.LENGTH_LONG);
        }
    }
}
