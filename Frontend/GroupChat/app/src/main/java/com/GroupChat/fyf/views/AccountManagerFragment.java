package com.GroupChat.fyf.views;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.GroupChat.fyf.utilities.BusProvider;
import com.GroupChat.fyf.utilities.PreferenceUtility;
import com.GroupChat.fyf.R;
import com.GroupChat.fyf.controllers.UsersController;
import com.GroupChat.fyf.model.User;
import com.squareup.otto.Subscribe;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AccountManagerFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AccountManagerFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AccountManagerFragment extends Fragment
{
    private TextView mTextFullNameView;
    private TextView mTextEmailView;
    private Button mButtonLogout;
    private String LOG_TAG = "AccountManagerFragment";

    // TODO: Rename and change types and number of parameters
    public static AccountManagerFragment newInstance(String param1, String param2)
    {
        AccountManagerFragment fragment = new AccountManagerFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public AccountManagerFragment()
    {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        BusProvider.getEventBus().register(this);
    }

    @Override
    public void onDestroy()
    {
        BusProvider.getEventBus().unregister(this);
        super.onDestroy();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_account_manager, container, false);
        mTextFullNameView = (TextView) view.findViewById(R.id.txt_fullname_value);
        mTextEmailView = (TextView) view.findViewById(R.id.txt_email_value);
        mButtonLogout = (Button) view.findViewById(R.id.buttonLogout);
        mButtonLogout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                logout();
            }
        });

        UsersController.getInstance(getActivity()).getUser(Long.valueOf(PreferenceUtility.getLoggedInID(getActivity())));
        return view;
    }

    @Subscribe
    public void getMessage(User user)
    {
        if(user != null)
        {
            mTextEmailView.setText(user.getEmail());
            mTextFullNameView.setText(user.getName());
        }
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener
    {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    private void logout ()
    {
        PreferenceUtility.logoutUser(getActivity());
        getActivity().getSupportFragmentManager().popBackStack();
    }

}
