package com.GroupChat.fyf.views;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.GroupChat.fyf.R;
import com.GroupChat.fyf.adapters.GroupAdapter;
import com.GroupChat.fyf.controllers.GroupsController;
import com.GroupChat.fyf.controllers.UsersController;
import com.GroupChat.fyf.model.Group;
import com.GroupChat.fyf.model.User;
import com.GroupChat.fyf.utilities.BusProvider;
import com.GroupChat.fyf.utilities.PreferenceUtility;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;

public class GroupsFragment extends Fragment
{
    private ListView mListView;
    private ListAdapter mListAdapter;
    private FloatingActionButton mFloatingActionButton;

    private final String LOG_TAG = getClass().getSimpleName();

    private ArrayList<Group> groups;
    private Group selectedGroup;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        String Id = PreferenceUtility.getLoggedInID(getActivity());
        //CHECK FOR LOGGED IN USER, IF NOT LOGGED IN REDIRECT TO LOGIN PAGE
        if(Id == "" || Id == "0")
        {
            Intent intent = new Intent(getActivity(), LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.fragment_groups, container, false);
        mListView = (ListView) rootView.findViewById(R.id.groupsView);

        mFloatingActionButton = (FloatingActionButton) rootView.findViewById(R.id.addGroup);
        mFloatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchCreateGroupScreen();
            }
        });

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Group group = (Group) adapterView.getItemAtPosition(i);
                MessagesFragment fragment = MessagesFragment.newInstance(group);
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, fragment)
                        .addToBackStack(null)
                        .commit();
            }
        });

        mListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                selectedGroup = (Group) adapterView.getItemAtPosition(i);
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                builder.setTitle("Remove Group");
                builder.setMessage("Would you like to delete the group " + selectedGroup.getName() + "?");

                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteGroup();
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
                return true;
            }
        });

        //initialize data

        String Id = PreferenceUtility.getLoggedInID(getActivity());
        if(Id == "" || Id == "0")
        {
            Intent intent = new Intent(getActivity(), LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
        }
        else
        {
            UsersController.getInstance(getActivity()).getUser(Long.valueOf(Id));
        }
        BusProvider.getEventBus().register(this);

        return rootView;
    }

    @Override
    public void onDestroyView()
    {
        BusProvider.getEventBus().unregister(this);
        super.onDestroyView();
    }

    private void launchCreateGroupScreen()
    {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        GroupCreateFragment groupCreateFragment = new GroupCreateFragment();
        fragmentManager.beginTransaction().setCustomAnimations(R.anim.abc_popup_enter, R.anim.abc_popup_exit)
                .replace(R.id.container, groupCreateFragment)
                .addToBackStack(null)
                .commit();
    }

    @Subscribe //onclick for each event
    public void getMessage(User user)
    {
        groups = user.getGroups();
        mListAdapter = new GroupAdapter(getActivity(), groups);
        mListView.setAdapter(mListAdapter);
    }

    @Subscribe
    public void getMessage(Group group)
    {
        UsersController.getInstance(getActivity()).getUser(Long.valueOf(PreferenceUtility.getLoggedInID(getActivity())));
    }

    public void deleteGroup()
    {
        GroupsController.getInstance(getActivity()).deleteGroup(String.valueOf(selectedGroup.getId()));
    }
}
