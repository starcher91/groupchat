package com.GroupChat.fyf.utilities;

import com.squareup.otto.Bus;

/**
 * Created by Stephen on 8/18/2015.
 */
public class BusProvider
{
    private static Bus eventBus;

    public static synchronized Bus getEventBus()
    {
        if(eventBus == null)
        {
            eventBus = new Bus();
        }
        return eventBus;
    }
}
