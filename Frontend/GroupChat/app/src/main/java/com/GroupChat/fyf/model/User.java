package com.GroupChat.fyf.model;

import java.util.ArrayList;

/**
 * Created by Stephen on 6/29/2015.
 */
public class User
{
    private long id;
    private String email;
    private String name;
    private ArrayList<Group> groups = new ArrayList<>();

    public User(long id, String email, String name)
    {
        this.id = id;
        this.email = email;
        this.name = name;
    }

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public ArrayList<Group> getGroups()
    {
        return groups;
    }

    public void setGroups(ArrayList<Group> groups)
    {
        this.groups = groups;
    }
}
