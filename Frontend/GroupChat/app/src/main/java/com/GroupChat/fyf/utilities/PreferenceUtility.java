package com.GroupChat.fyf.utilities;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Stephen on 6/19/2015.
 */
public class PreferenceUtility
{
    static final String PREF_LOGGEDIN_EMAIL = "logged_in_email";

    public static void setLoggedInUserID(Context context, long id)
    {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.putString(PREF_LOGGEDIN_EMAIL, String.valueOf(id)).commit();
    }

    public static String getLoggedInID(Context context)
    {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(PREF_LOGGEDIN_EMAIL, "");
    }

    public static void logoutUser(Context context)
    {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.remove(PREF_LOGGEDIN_EMAIL).commit();
    }
}
