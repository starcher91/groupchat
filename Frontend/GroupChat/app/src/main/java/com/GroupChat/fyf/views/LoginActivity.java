package com.GroupChat.fyf.views;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.GroupChat.fyf.R;
import com.GroupChat.fyf.utilities.BusProvider;
import com.GroupChat.fyf.utilities.UIUtility;
import com.GroupChat.fyf.controllers.UsersController;
import com.GroupChat.fyf.model.User;
import com.squareup.otto.Subscribe;

public class LoginActivity extends AppCompatActivity
{

    EditText mEmailEditText;
    EditText mPasswordEditText;
    Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mEmailEditText = (EditText) findViewById(R.id.txt_email);
        mPasswordEditText = (EditText) findViewById(R.id.txt_password);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        BusProvider.getEventBus().register(this);
    }

    @Override
    public void onDestroy()
    {
        BusProvider.getEventBus().unregister(this);
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
        {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void login(View view)
    {
        String email = mEmailEditText.getText().toString().trim();
        String password = mPasswordEditText.getText().toString().trim();

        if(!UIUtility.isValidEmail(email))
        {
            Toast.makeText(this, "Invalid Email address entered!", Toast.LENGTH_SHORT).show();
            return;
        }

        User user = UsersController.getInstance(getBaseContext()).login(email, password);
    }

    public void goToRegister (View view)
    {
        Intent intent = new Intent(this, RegisterActivity.class);
        this.finish();
        startActivity(intent);
    }

    @Subscribe
    public void getMessage(User user)
    {
        if(user != null)
        {
            Toast.makeText(this, "Login Successful!", Toast.LENGTH_SHORT).show();
            this.finish();
        }
        else
        {
            Toast.makeText(this, "Login Unsuccessful, Please try again!", Toast.LENGTH_LONG).show();
        }
    }
}
