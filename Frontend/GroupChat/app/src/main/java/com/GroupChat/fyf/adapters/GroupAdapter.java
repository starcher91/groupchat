package com.GroupChat.fyf.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.GroupChat.fyf.R;
import com.GroupChat.fyf.model.Group;
import com.GroupChat.fyf.model.Group;
import com.GroupChat.fyf.utilities.BusProvider;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Stephen on 6/30/2015.
 */
public class GroupAdapter extends ArrayAdapter<Group>
{
    public GroupAdapter(Context context, ArrayList<Group> Groups)
    {
        super(context, R.layout.cardview_groups, Groups);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        Group group = getItem(position);
        if(convertView == null)
        {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.cardview_groups, parent, false);
        }
        TextView groupnameText = (TextView) convertView.findViewById(R.id.group_name);
        groupnameText.setText(group.getName());
        return convertView;
    }
}
