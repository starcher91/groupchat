package com.GroupChat.fyf.views;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;

import com.GroupChat.fyf.R;
import com.GroupChat.fyf.adapters.UserAdapter;
import com.GroupChat.fyf.controllers.GroupsController;
import com.GroupChat.fyf.controllers.UsersController;
import com.GroupChat.fyf.model.Group;
import com.GroupChat.fyf.model.User;
import com.GroupChat.fyf.utilities.BusProvider;
import com.GroupChat.fyf.utilities.UIUtility;
import com.squareup.otto.Subscribe;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Stephen on 8/17/2015.
 */
public class GroupEditFragment extends Fragment
{
    private EditText mGroupName;
    private Button mCreate;
    private LinearLayout mLinearLayout;
    private ListView mUserListView;
    private UserAdapter mUserAdapter;

    private final String LOG_TAG = getClass().getSimpleName();

    private Group mGroup = null;
    private User selectedUser = null;
    private static final String PARAM_ID_KEY = "Id";

    public static GroupEditFragment newInstance(Group group)
    {
        GroupEditFragment fragment = new GroupEditFragment();
        Bundle args = new Bundle();
        args.putLong(PARAM_ID_KEY, group.getId());
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        BusProvider.getEventBus().register(this);
        if(getArguments() != null)
        {
            long groupId = getArguments().getLong(PARAM_ID_KEY);
            GroupsController.getInstance(getActivity()).getGroup(groupId);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_group_edit, container, false);

        mGroupName = (EditText) rootView.findViewById(R.id.group_name);
        mCreate = (Button) rootView.findViewById(R.id.btn_group_edit);
        mUserListView = (ListView) rootView.findViewById(R.id.lv_group_users);

        mCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = mGroupName.getText().toString();
                GroupsController.getInstance(getActivity()).updateGroup(mGroup.getId(), name);
            }
        });

        mUserListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                selectedUser = (User) adapterView.getItemAtPosition(i);
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                builder.setTitle("Remove User");
                builder.setMessage("Would you like to remove " + selectedUser.getName() + " from the group?");

                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        removeUserFromGroup();
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
                return true;
            }
        });

        return rootView;
    }

    @Override
    public void onDestroy()
    {
        BusProvider.getEventBus().unregister(this);
        super.onDestroy();
    }

    public void removeUserFromGroup()
    {
        if(selectedUser != null && mGroup != null)
        {
            UsersController.getInstance(getActivity()).removeUserFromGroup(String.valueOf(selectedUser.getId()), String.valueOf(mGroup.getId()));
        }
    }

    @Subscribe
    public void getMessage(Group group)
    {
        if(group != null)
        {
            if(mGroup == null) //first opening of fragment
            {
                mGroup = group;
                mGroupName.setText(mGroup.getName());
                mUserAdapter = new UserAdapter(getActivity(), mGroup.getUsers());
                mUserListView.setAdapter(mUserAdapter);
            }
            else //save of group
            {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        }
    }

    @Subscribe
    public void getMessage(User user)
    {
        //reload users in group
        GroupsController.getInstance(getActivity()).getGroup(mGroup.getId());
        mGroup = null; //mark for reinitialization
    }
}
