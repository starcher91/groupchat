package com.GroupChat.fyf.utilities;

import android.text.TextUtils;
import android.util.Patterns;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Stephen on 6/17/2015.
 */
public class UIUtility
{
    public final static boolean isValidEmail(String target)
    {
        if(TextUtils.isEmpty(target))
        {
            return false;
        }
        else
        {
            return Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public final static String getDate(long milliseconds, String dateFormat)
    {
        SimpleDateFormat format = new SimpleDateFormat(dateFormat);

        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(milliseconds);
        return format.format(cal.getTime());
    }
}
