package com.GroupChat.fyf.views;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.GroupChat.fyf.R;
import com.GroupChat.fyf.adapters.MessageAdapter;
import com.GroupChat.fyf.controllers.GroupsController;
import com.GroupChat.fyf.model.Group;
import com.GroupChat.fyf.model.Message;
import com.GroupChat.fyf.utilities.BusProvider;
import com.GroupChat.fyf.utilities.PreferenceUtility;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
public class MessagesFragment extends Fragment
{
    private static final String ARG_PARAM1 = "GroupId";
    private long groupId;
    private final String LOG_TAG = this.getClass().getSimpleName();

    private Group group;
    private ArrayAdapter<Message> mListAdapter;
    private ListView mListView;
    private Button mSendButton;
    private EditText mNewMessage;

    public static MessagesFragment newInstance(Group group)
    {
        MessagesFragment fragment = new MessagesFragment();
        Bundle args = new Bundle();
        args.putLong(ARG_PARAM1, group.getId());
        fragment.setArguments(args);
        return fragment;
    }

    public MessagesFragment()
    {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null)
        {
            groupId = getArguments().getLong(ARG_PARAM1);
        }
        GroupsController.getInstance(getActivity()).getGroup(groupId);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        View parent = inflater.inflate(R.layout.fragment_messages, container, false);
        mListView = (ListView) parent.findViewById(R.id.lv_message_list);
        mSendButton = (Button) parent.findViewById(R.id.btn_send_message);
        mNewMessage = (EditText) parent.findViewById(R.id.et_message_content);

        mSendButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                sendMessage();
            }
        });

        BusProvider.getEventBus().register(this);
        return parent;
    }

    @Override
    public void onResume()
    {
        if(this.group != null) //reload messages
        {
            mListAdapter = new MessageAdapter(getActivity(), this.group.getMessages());
            mListView.setAdapter(mListAdapter);
        }
        super.onResume();
    }

    @Override
    public void onDestroyView()
    {
        BusProvider.getEventBus().unregister(this);
        super.onDestroyView();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        menu.clear();
        inflater.inflate(R.menu.menu_messages_fragment, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        if(id == R.id.action_edit_group)
        {
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            GroupEditFragment groupEditFragment = GroupEditFragment.newInstance(this.group);
            fragmentManager.beginTransaction().setCustomAnimations(R.anim.abc_popup_enter, R.anim.abc_popup_exit)
                    .replace(R.id.container, groupEditFragment)
                    .addToBackStack(null)
                    .commit();
        }
        else if(id == R.id.action_add_group_users)
        {
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            UserGroupAdd userGroupAdd = UserGroupAdd.newInstance(this.group);
            fragmentManager.beginTransaction().setCustomAnimations(R.anim.abc_popup_enter, R.anim.abc_popup_exit)
                    .replace(R.id.container, userGroupAdd)
                    .addToBackStack(null)
                    .commit();
        }

        return super.onOptionsItemSelected(item);
    }

    public void sendMessage()
    {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        GroupsController.getInstance(getActivity()).sendMessage(PreferenceUtility.getLoggedInID(getActivity()), this.group, mNewMessage.getText().toString());
        mNewMessage.setText("");
    }

    @Subscribe
    public void getMessage(Group group)
    {
        this.group = group;
        GroupsController.getInstance(getActivity()).getGroupMessages(PreferenceUtility.getLoggedInID(getActivity()), group);
    }

    @Subscribe
    public void getMessage(ArrayList<Message> messages)
    {
        this.group.setMessages(messages);
        mListAdapter = new MessageAdapter(getActivity(), messages);
        mListView.setAdapter(mListAdapter);
        mListView.setSelection(mListAdapter.getCount() - 1);
    }

    @Subscribe
    public void getMessage(Message message)
    {
        //reload messages once callback is finished
        GroupsController.getInstance(getActivity()).getGroupMessages(PreferenceUtility.getLoggedInID(getActivity()), group);
    }
}
