package com.GroupChat.fyf.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.GroupChat.fyf.R;
import com.GroupChat.fyf.model.Message;
import com.GroupChat.fyf.model.User;
import com.GroupChat.fyf.utilities.PreferenceUtility;

import java.util.ArrayList;

/**
 * Created by Stephen on 12/5/2015.
 */
public class UserAdapter extends ArrayAdapter<User>
{
    public UserAdapter(Context context, ArrayList<User> users)
    {
        super(context, R.layout.current_user_message_list_item, users);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        User user = getItem(position);
        if(convertView == null)
        {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.cardview_users, parent, false);
        }
        TextView usernameText = (TextView) convertView.findViewById(R.id.user_name);
        TextView userEmailText = (TextView) convertView.findViewById(R.id.tv_user_email);
        usernameText.setText(user.getName());
        userEmailText.setText(user.getEmail());
        return convertView;
    }
}
