package com.GroupChat.fyf.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Stephen on 6/17/2015.
 */
public class FyfDbHelper extends SQLiteOpenHelper
{
    private static final int DATABASE_VERSION = 3;
    public static final String DATABASE_NAME = "fyf.db";

    public FyfDbHelper(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase sqLiteDatabase)
    {
        final String SQL_CREATE_USERS_TABLE = "CREATE TABLE IF NOT EXISTS " + FyfContract.UserEntry.TABLE_NAME + " (" +
                FyfContract.UserEntry._ID + " INTEGER PRIMARY KEY," +
                FyfContract.UserEntry.COLUMN_EMAIL + " TEXT NOT NULL," +
                FyfContract.UserEntry.COLUMN_NAME + " TEXT NOT NULL);";

        final String SQL_CREATE_GROUPS_TABLE = "CREATE TABLE IF NOT EXISTS " + FyfContract.GroupEntry.TABLE_NAME + " (" +
                FyfContract.GroupEntry._ID + " INTEGER PRIMARY KEY," +
                FyfContract.GroupEntry.COLUMN_NAME + " TEXT NOT NULL, " +
                FyfContract.GroupEntry.COLUMN_IS_PRIVATE + " INTEGER NOT NULL," +
                FyfContract.GroupEntry.COLUMN_END_DATE + " INTEGER NOT NULL);"; //INTEGER REPRESENTING NUMBER OF SECONDS SINCE 1/1/1970

        final String SQL_CREATE_USERS_GROUPS_TABLE = "CREATE TABLE IF NOT EXISTS " + FyfContract.UsersGroupsEntry.TABLE_NAME + " (" +
                FyfContract.UsersGroupsEntry._ID + " INTEGER PRIMARY KEY," +
                FyfContract.UsersGroupsEntry.COLUMN_USER_ID + " INTEGER NOT NULL, " +
                FyfContract.UsersGroupsEntry.COLUMN_GROUP_ID + " INTEGER NOT NULL," +

                " FOREIGN KEY (" + FyfContract.UsersGroupsEntry.COLUMN_USER_ID + ") REFERENCES " +
                FyfContract.UserEntry.TABLE_NAME + " (" + FyfContract.UserEntry._ID + "), " +
                " FOREIGN KEY (" + FyfContract.UsersGroupsEntry.COLUMN_GROUP_ID + ") REFERENCES " +
                FyfContract.GroupEntry.TABLE_NAME + " (" + FyfContract.GroupEntry._ID + "));";

        sqLiteDatabase.execSQL(SQL_CREATE_USERS_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_GROUPS_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_USERS_GROUPS_TABLE);
    }

    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion)
    {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + FyfContract.UsersGroupsEntry.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + FyfContract.UserEntry.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + FyfContract.GroupEntry.TABLE_NAME);
        onCreate(sqLiteDatabase);
    }
}
