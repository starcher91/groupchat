package com.GroupChat.fyf.controllers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Base64;
import android.util.Log;

import com.GroupChat.fyf.model.Group;
import com.GroupChat.fyf.model.Message;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.GroupChat.fyf.database.FyfContract;
import com.GroupChat.fyf.database.FyfDbHelper;
import com.GroupChat.fyf.model.User;
import com.GroupChat.fyf.utilities.BusProvider;
import com.GroupChat.fyf.utilities.NetworkRequestSingleton;
import com.GroupChat.fyf.utilities.PreferenceUtility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Created by Stephen on 6/29/2015.
 */
public class UsersController
{
    private Hashtable <Long, User> mUsers = new Hashtable<Long, User>();
    private static UsersController mController;
    private static Context mContext;
    private static FyfDbHelper mFyfDbHelper;
    private static String LOG_TAG = "UsersController";
    private long tempUserId;

    public static synchronized UsersController getInstance(Context context)
    {
        if(mController == null)
        {
            mController = new UsersController(context);
        }
        return mController;
    }

    private UsersController(Context context)
    {
        mContext = context;
        mFyfDbHelper = new FyfDbHelper(context);
    }

    private String convertToHex(byte[] data) throws java.io.IOException
    {
        StringBuffer sb = new StringBuffer();
        String hex = null;

        hex = Base64.encodeToString(data, 0, data.length, 0);
        sb.append(hex);
        return sb.toString();
    }

    private String hashPassword(String password)
    {
        MessageDigest digest = null;
        try
        {
            digest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
            return ""; //catch in UI
        }

        try
        {
            digest.update(password.getBytes("ASCII"));
        }
        catch(UnsupportedEncodingException e)
        {
            e.printStackTrace();
            return ""; //catch in UI
        }

        byte[] data = digest.digest();
        String hashedPassword = null;
        try
        {
            hashedPassword = convertToHex(data);
        }
        catch(IOException e)
        {
            e.printStackTrace();
            return "";
        }
        return hashedPassword;
    }

    public void createUser(String name, String password, String email)
    {
        String hashedPassword = hashPassword(password);
        String requestURL = Uri.parse(NetworkRequestSingleton.ServerURL + "/users")
                        .buildUpon()
                        .appendQueryParameter("name", name)
                        .appendQueryParameter("email", email)
                        .appendQueryParameter("password", hashedPassword)
                        .build().toString();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.POST, requestURL, (String)null,
                new Response.Listener<JSONObject>() {
                    User user = null;
                    @Override
                    public void onResponse(JSONObject response) {
                        long id = -1;
                        String email = null;
                        String name = null;
                        try {
                            id = response.getLong("id");
                            email = response.getString("email");
                            name = response.getString("name");
                        }
                        catch(JSONException e)
                        {
                            e.printStackTrace();
                            Log.e(LOG_TAG, "Json Object doesn't match");
                        }
                        if(id > -1)
                        {
                            user = new User(id, email, name);
//                            mUsers.put(user.getId(), user);

//                            PreferenceUtility.setLoggedInUserID(mContext, id);
//
//                            SQLiteDatabase db = mFyfDbHelper.getWritableDatabase();
//
//                            ContentValues cv = new ContentValues();
//                            cv.put(FyfContract.UserEntry._ID, id);
//                            cv.put(FyfContract.UserEntry.COLUMN_NAME, name);
//                            cv.put(FyfContract.UserEntry.COLUMN_EMAIL, email);
//                            cv.put(FyfContract.UserEntry.COLUMN_LATITUDE, 0);
//                            cv.put(FyfContract.UserEntry.COLUMN_LONGITUDE, 0);
//
//                            tempUserId = db.replace(FyfContract.UserEntry.TABLE_NAME
//                                    ,null
//                                    ,cv);
//
//                            db.close();
                        }
                        BusProvider.getEventBus().post(user);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO Auto-generated method stub

            }
        });
        NetworkRequestSingleton.getInstance(mContext).addToRequestQueue(jsonObjectRequest);
    }

    public User getUser(long id)
    {
        String requestURL = Uri.parse(NetworkRequestSingleton.ServerURL + "/users/" + id).buildUpon().build().toString();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET, requestURL, (String)null,
                new Response.Listener<JSONObject>() {
                    User user = null;
                    @Override
                    public void onResponse(JSONObject response) {
                        long id = -1;
                        String email = null;
                        String name = null;
                        long groupId = -1;
                        String groupName = null;
                        ArrayList<Group> groups = new ArrayList<Group>();
                        try {
                            id = response.getLong("id");
                            email = response.getString("email");
                            name = response.getString("name");
                            JSONArray jsonGroups = response.getJSONArray("Groups");
                            for(int i = 0; i < jsonGroups.length(); i++)
                            {
                                JSONObject jsonObject = jsonGroups.getJSONObject(i);
                                groups.add(new Group(jsonObject.getLong("id"), jsonObject.getString("name")));
                            }
                        }
                        catch(JSONException e)
                        {
                            e.printStackTrace();
                            Log.e(LOG_TAG, "Json Object doesn't match");
                        }
                        user = new User(id, email, name);
                        user.setGroups(groups);

                        BusProvider.getEventBus().post(user);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO Auto-generated method stub

            }
        });
        NetworkRequestSingleton.getInstance(mContext).addToRequestQueue(jsonObjectRequest);
//        return mUsers.get(id);
        return null;
    }

    public User login(String email, String password)
    {
        String hashedPassword = hashPassword(password);
        String requestURL = Uri.parse(NetworkRequestSingleton.ServerURL + "/users")
                .buildUpon()
                .appendQueryParameter("email", email)
                .appendQueryParameter("password", hashedPassword)
                .build().toString();

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET, requestURL, (String)null,
                new Response.Listener<JSONObject>() {
                    User user = null;
                    @Override
                    public void onResponse(JSONObject response) {
                        long id = -1;
                        String email = null;
                        String name = null;
                        try {
                            id = response.getLong("id");
                            email = response.getString("email");
                            name = response.getString("name");
                        }
                        catch(JSONException e)
                        {
                            e.printStackTrace();
                            Log.e(LOG_TAG, "Json Object doesn't match");
                        }
                        user = new User(id, email, name);
                        mUsers.put(user.getId(), user);

                        PreferenceUtility.setLoggedInUserID(mContext, id);

                        BusProvider.getEventBus().post(user);

//                        SQLiteDatabase db = mFyfDbHelper.getWritableDatabase();
//
//                        ContentValues cv = new ContentValues();
//                        cv.put(FyfContract.UserEntry._ID, id);
//                        cv.put(FyfContract.UserEntry.COLUMN_NAME, name);
//                        cv.put(FyfContract.UserEntry.COLUMN_EMAIL, email);
//                        cv.put(FyfContract.UserEntry.COLUMN_LATITUDE, 0);
//                        cv.put(FyfContract.UserEntry.COLUMN_LONGITUDE, 0);
//
//                        tempUserId = id;
//
//                        db.replace(FyfContract.UserEntry.TABLE_NAME
//                                ,null
//                                ,cv);
//
//                        db.close();
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO Auto-generated method stub

            }
        });
        NetworkRequestSingleton.getInstance(mContext).addToRequestQueue(jsonObjectRequest);
        return mUsers.get(tempUserId);
    }

    public User updateUser(long id, String name, String email, double latitude, double longitude)
    {
        SQLiteDatabase db = mFyfDbHelper.getWritableDatabase();

        ContentValues cv = new ContentValues();

        cv.put(FyfContract.UserEntry.COLUMN_NAME, name);
        cv.put(FyfContract.UserEntry.COLUMN_EMAIL, email);
        cv.put(FyfContract.UserEntry.COLUMN_LATITUDE, latitude);
        cv.put(FyfContract.UserEntry.COLUMN_LONGITUDE, longitude);

        db.update(FyfContract.UserEntry.TABLE_NAME
                , cv
                , FyfContract.UserEntry._ID + " = ?"
                ,new String[] {String.valueOf(id)});
        db.close();

        return getUser(id);
    }

    public void addUserToGroup(String groupId, String userId)
    {
        String requestURL = Uri.parse(NetworkRequestSingleton.ServerURL + "/users")
                .buildUpon()
                .appendPath(userId)
                .appendPath("groups")
                .appendPath(groupId)
                .build().toString();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.POST, requestURL, (String)null,
                new Response.Listener<JSONObject>() {
                    User user = null;
                    @Override
                    public void onResponse(JSONObject response) {
                        long id = -1;
                        String email = null;
                        String name = null;
                        try {
                            id = response.getLong("id");
                            email = response.getString("email");
                            name = response.getString("name");
                        }
                        catch(JSONException e)
                        {
                            e.printStackTrace();
                            Log.e(LOG_TAG, "Json Object doesn't match");
                        }
                        if(id > -1)
                        {
                            user = new User(id, email, name);
                        }
                        BusProvider.getEventBus().post(user);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(LOG_TAG, "Adding User failed!");
            }
        });
        NetworkRequestSingleton.getInstance(mContext).addToRequestQueue(jsonObjectRequest);
    }

    public void removeUserFromGroup(String userId, String groupId)
    {
        String requestURL = Uri.parse(NetworkRequestSingleton.ServerURL)
                .buildUpon()
                .appendPath("users")
                .appendPath(userId)
                .appendPath("groups")
                .appendPath(groupId)
                .build().toString();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.DELETE, requestURL, (String)null,
                new Response.Listener<JSONObject>() {
                    User user = null;
                    @Override
                    public void onResponse(JSONObject response) {
                        long id = -1;
                        String email = null;
                        String name = null;
                        long groupId = -1;
                        String groupName = null;
                        ArrayList<Group> groups = new ArrayList<Group>();
                        try {
                            id = response.getLong("id");
                            email = response.getString("email");
                            name = response.getString("name");
                            JSONArray jsonGroups = response.getJSONArray("Groups");
                            for(int i = 0; i < jsonGroups.length(); i++)
                            {
                                JSONObject jsonObject = jsonGroups.getJSONObject(i);
                                groupId = jsonObject.getLong("id");
                                groupName = jsonObject.getString("name");
                                Group group = new Group(groupId, groupName);
                                groups.add(group);
                            }
                        }
                        catch(JSONException e)
                        {
                            e.printStackTrace();
                            Log.e(LOG_TAG, "Json Object doesn't match");
                        }
                        user = new User(id, email, name);
                        user.setGroups(groups);
                        BusProvider.getEventBus().post(user);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO Auto-generated method stub

            }
        });
        NetworkRequestSingleton.getInstance(mContext).addToRequestQueue(jsonObjectRequest);
//        return mUsers.get(id);
    }

    public void searchUsers(String query)
    {
        String requestURL = Uri.parse(NetworkRequestSingleton.ServerURL + "/users")
                .buildUpon()
                .appendQueryParameter("search", query)
                .build().toString();
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(
                Request.Method.GET, requestURL, (String)null,
                new Response.Listener<JSONArray>() {
                    ArrayList<User> users = new ArrayList<User>();
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            User user;
                            Group group;
                            for(int i = 0; i < response.length(); i++)
                            {
                                JSONObject userObject = (JSONObject) response.get(i);
                                JSONArray groupArray = userObject.getJSONArray("Groups");
                                user = new User(userObject.getLong("id"), userObject.getString("email"), userObject.getString("name"));
                                for(int j = 0; j < groupArray.length(); j++)
                                {
                                    JSONObject groupObject = (JSONObject) groupArray.get(j);
                                    group = new Group(groupObject.getLong("id"), groupObject.getString("name"));
                                    user.getGroups().add(group);
                                }
                                users.add(user);
                            }
                        }
                        catch(JSONException e)
                        {
                            e.printStackTrace();
                            Log.e(LOG_TAG, "Json Object doesn't match");
                        }
                        BusProvider.getEventBus().post(users);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO Auto-generated method stub

            }
        });
        NetworkRequestSingleton.getInstance(mContext).addToRequestQueue(jsonArrayRequest);
    }
}
