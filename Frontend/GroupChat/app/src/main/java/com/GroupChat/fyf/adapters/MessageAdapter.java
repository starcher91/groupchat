package com.GroupChat.fyf.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.GroupChat.fyf.R;
import com.GroupChat.fyf.model.Message;
import com.GroupChat.fyf.utilities.PreferenceUtility;

import java.util.ArrayList;

/**
 * Created by Stephen on 10/27/2015.
 */
public class MessageAdapter extends ArrayAdapter<Message>
{
    public MessageAdapter(Context context, ArrayList<Message> messages)
    {
        super(context, R.layout.current_user_message_list_item, messages);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        Message message = getItem(position);
        if(convertView == null)
        {
            if(Long.valueOf(PreferenceUtility.getLoggedInID(getContext())) == message.getUser().getId())
            {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.current_user_message_list_item, parent, false);
            }
            else
            {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.other_user_message_list_item, parent, false);
            }
        }
        TextView messageText = (TextView) convertView.findViewById(R.id.tv_message_text);
        TextView userText = (TextView) convertView.findViewById(R.id.tv_message_user);
        messageText.setText(message.getText());
        userText.setText(message.getUser().getName());
        return convertView;
    }
}
