package com.GroupChat.fyf.model;

/**
 * Created by Stephen on 10/27/2015.
 */
public class Message
{
    private long id;
    private String text;
    private User user;
    private Group group;

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public String getText()
    {
        return text;
    }

    public void setText(String text)
    {
        this.text = text;
    }

    public Message(long id, String text, User user, Group group)
    {
        this.id = id;
        this.text = text;
        this.user = user;
        this.group = group;
    }

    public Message(long id, String text, User user)
    {
        this.id = id;
        this.text = text;
        this.user = user;
    }

    @Override
    public String toString()
    {
        return this.text;
    }

    public void setUser(User user)
    {
        this.user = user;
    }

    public User getUser()
    {
        return this.user;
    }

    public Group getGroup()
    {
        return this.group;
    }

    public void setGroup(Group group)
    {
        this.group = group;
    }
}
