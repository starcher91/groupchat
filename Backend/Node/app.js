var express = require("express");
var path = require("path");

var models = require("./models");
var routes = require("./routes/index");
var users = require("./routes/users");
var groups = require("./routes/groups");
var app = express();


app.use('/', routes);
app.use('/users', users);
app.use('/groups', groups);

module.exports = app;