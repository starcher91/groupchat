module.exports = function(sequelize, DataTypes)
{
	var User = sequelize.define('User', {
			name: {type: DataTypes.STRING, field : 'name'},
			email: DataTypes.STRING,
			password: DataTypes.STRING
		}, 
		{
			timestamps: true,
			classMethods : {
				associate: function(models) {
					User.belongsToMany(models.Group, {through : 'UserGroup'});
					User.hasMany(models.Message);
				}
			},
			instanceMethods : {
				toJSON: function() { //overload the toJSON to remove the password from the return set
					var values = this.get();
					delete values.password;
					return values;
				}
			}
		}
	);
	return User;
};