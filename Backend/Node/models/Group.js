module.exports = function(sequelize, DataTypes)
{
	var Group = sequelize.define('Group', {
		name: {type: DataTypes.STRING, field: 'name'}
	},
	{
		timestamps:true,
		classMethods : {
			associate: function(models) {
				Group.belongsToMany(models.User, {through : 'UserGroup'});
				Group.hasMany(models.Message);
			}
		}
	});

	return Group;
};