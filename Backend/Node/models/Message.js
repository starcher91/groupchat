module.exports = function(sequelize, DataTypes)
{
	var Message = sequelize.define('Message', {
			text: DataTypes.STRING
		}, 
		{
			timestamps: true,
			classMethods : {
				associate: function(models) {
					Message.belongsTo(models.User, {onDelete: "CASCADE"});
					Message.belongsTo(models.Group, {onDelete: "CASCADE"});
				}
			}
		}
	);
	return Message;
};