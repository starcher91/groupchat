var models = require('../models');
var express = require('express');
var router = express.Router();

router.get('/', function(req, res) {
	models.User.findAll({
		include: [models.Group]
	}).then(function(users) {
		res.json(users);
	});
});

module.exports = router;