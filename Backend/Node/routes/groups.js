var models = require('../models');
var express = require('express');
var router = express.Router();

router.post('/', function(req, res) {
	models.Group.create({
		name: req.query.name
	}).then(function(group) {
		res.json(group);
	});
});

router.get('/:group_id', function(req, res) {
	models.Group.find({
		where: {
			id : req.params.group_id
		},
		include:[{model: models.User}]
	}).then(function(group) {
		res.json(group);
	});
});

router.delete('/:group_id', function(req, res) {
	models.Group.destroy( {
		where : {
			id : req.params.group_id
		}
	}).then(function() {
		res.json({id: -1});
	});
});

router.put('/:group_id', function(req, res) {
	models.Group.update( {
		name:req.query.name
	}, {
		where : {
			id : req.params.group_id
		}
	}).then(function(group) {
		models.Group.find({
		where: {
			id : req.params.group_id
		},
		include:[{model: models.User}]
		}).then(function(group) {
			res.json(group);
		});
	});
});

router.get('/:group_id/messages', function(req, res) {
	models.Message.findAll({
		where: {
			GroupId : req.params.group_id
		}
	}).then(function(messages) {
		res.json(messages);
	});
});

module.exports = router;