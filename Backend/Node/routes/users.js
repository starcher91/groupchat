var models = require('../models');
var express = require('express');
var router = express.Router();

router.post('/', function(req, res) {
	models.User.create({
		name: req.query.name,
		email: req.query.email,
		password: req.query.password
	}).then(function(user) {
		res.json(user);
	});
});

router.get('/:user_id', getUser);

//login or search
router.get('/', function(req, res) {
	if(req.query.password)
	{
		models.User.find({
		where: {
			email : req.query.email,
			password : req.query.password
		}
		}).then(function(user) {
			if(user)
			{
				res.json(user);
			}
			else
			{
				var response = {error : "Invalid Login!"};
				res.json(response);
			}
		})
	}
	else
	{
		models.User.findAll({
		where : {
			email : { $like: "%" + req.query.search + "%" }
		},
		include: [{model:models.Group}]
		}).then(function(users) {
			res.json(users);
		});
	}
});

router.delete('/:user_id', function(req, res) {
	models.User.destroy( {
		where : {
			id : req.params.user_id
		}
	}).then(function() {
		res.send('User ' + req.params.user_id + ' has been deleted!')
	});
});

router.put('/:user_id', function(req, res) {
	models.User.update( {
		name:req.query.name,
		email:req.query.email
	}, {
		where : {
			id : req.params.user_id
		}
	}).then(function(user) {
		res.json(user);
	});
});

//////////////////////////////////////////////////////////////////////////
//							UserGroup Routes
//////////////////////////////////////////////////////////////////////////

router.post('/:user_id/groups/:group_id', function(req, res, next) {
	models.User.find({
		where: {
			id: req.params.user_id
		}
	}).then(function(user) {
		user.addGroup(req.params.group_id).then(function() { next(); });
	});
}, getUser);

router.delete('/:user_id/groups/:group_id', function(req, res, next) {
	models.User.find({
		where: {
			id: req.params.user_id
		}
	}).then(function(user) {
		user.removeGroup(req.params.group_id).then(function() { next(); });
	});
}, getUser);

function getUser(req, res)
{
	models.User.find({
		where: {
			id : req.params.user_id
		},
		include: [{model:models.Group}]
	}).then(function(users) {
		res.json(users);
	});
}

//////////////////////////////////////////////////////////////////////////
//							MESSAGES ROUTES
//////////////////////////////////////////////////////////////////////////

router.post('/:user_id/groups/:group_id/messages/', function(req, res) {
	models.Message.create({
		text: req.query.text,
		UserId: req.params.user_id,
		GroupId: req.params.group_id
	}).then(function(message) {
		models.Message.find({ //DONE TO RETRIEVE USER DATA AS WELL
			where : {
				id : message.id
			},
			include : [{model: models.User}]
		}).then(function(messageWithUser) {
			console.log(messageWithUser);
			res.json(messageWithUser);
		});
	});
});

router.get('/:user_id/groups/:group_id/messages/:message_id', function(req, res) {
	models.Message.find({
		where: {
			id : req.params.message_id
		},
		include:[{model: models.User}, {model: models.Group}]
	}).then(function(message) {
		res.json(message);
	});
});

router.get('/:user_id/groups/:group_id/messages', function(req, res) {
	models.Message.findAll({
		where: {
			GroupId : req.params.group_id
		},
		include: [{model: models.User}, {model:models.Group}]
	}).then(function(messages) {
		res.json(messages);
	});
});

router.delete('/:user_id/groups/:group_id/messages/:message_id', function(req, res) {
	models.Message.destroy( {
		where : {
			id : req.params.message_id
		}
	}).then(function() {
		res.send('Message ' + req.params.message_id + ' has been deleted!')
	});
});

router.put('/:user_id/groups/:group_id/messages/:message_id', function(req, res) {
	models.Message.update( {
		name:req.query.name
	}, {
		where : {
			id : req.params.message_id
		}
	}).then(function(message) {
		res.json(message);
	});
});

module.exports = router;