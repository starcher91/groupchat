import os
import sys

def runInDirectory(directoryName, files):
	f = files

	for fileName in f:
		if(".sql" in fileName):
			command = "psql -d " + sys.argv[1] + " -a -f \"" + directoryName + "\\" + fileName + "\" -U postgres"
			#print(command) #line kept for debugging
			file = open(directoryName +"/" + fileName)
			str = file.read()
			createLocation = str.find("CREATE")
			str = str[createLocation:] #removes header bytes
			file = open(directoryName +"/" + fileName, "w")
			file.write(str)
			file.close()
			os.system(command)
	return

rootdir = os.getcwd()

for root, dirs, files in os.walk(rootdir):
	runInDirectory(root, files)