﻿DROP TABLE IF EXISTS messages CASCADE;
DROP TABLE IF EXISTS users_groups CASCADE;
DROP TABLE IF EXISTS users CASCADE;
DROP TABLE IF EXISTS groups CASCADE;

CREATE TABLE users (
	ID serial PRIMARY KEY
	,email text NOT NULL
	,password text NOT NULL
	,name text
);

CREATE TABLE groups (
	ID serial PRIMARY KEY
	,name varchar(50)
	,is_private boolean
	,end_date timestamp
);

CREATE TABLE users_groups (
	ID serial PRIMARY KEY
	,user_id int REFERENCES users(ID)
	,group_id int REFERENCES groups(ID)
	,is_admin char(1)
);

CREATE TABLE messages (
	ID serial PRIMARY KEY
	,user_group_id int REFERENCES users_groups(ID)
	,content text
	,sent_date timestamp
);