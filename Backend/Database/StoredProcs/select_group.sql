CREATE OR REPLACE FUNCTION select_group(
    parm_id integer,
    parm_name text,
    parm_is_private boolean,
    parm_end_date double precision)
  RETURNS TABLE(id int, name text, is_private boolean, end_date double precision) AS
$$
	SELECT id, name, is_private, EXTRACT(EPOCH FROM end_date) as end_date
	FROM groups
	WHERE (parm_id is NULL or id = parm_id)
	and (parm_name is null or name = parm_name)
	and (parm_is_private is null or parm_is_private = is_private)
	and (parm_end_date is null or parm_end_date = EXTRACT(EPOCH FROM end_date));
$$
  LANGUAGE SQL;

