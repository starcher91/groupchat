CREATE OR REPLACE FUNCTION update_user(parm_id int, parm_email text, parm_password text, parm_name text)
returns void as $$
begin
	UPDATE users
	set email = coalesce(parm_email, email),
	    password = coalesce(parm_password, password),
	    name = coalesce(parm_name, name)
	where id = parm_id;
end;
$$ LANGUAGE plpgsql