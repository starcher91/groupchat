﻿CREATE OR REPLACE FUNCTION update_message(parm_id int, parm_user_group_id int, parm_content text, parm_sent_date double precision)
returns TABLE(id int, user_group_id int, content text, sent_date double precision) as $$
begin
	UPDATE messages
	set user_group_id = coalesce(parm_user_group_id, user_group_id),
	    content = coalesce(parm_content, content),
	    sent_date = coalesce(parm_sent_date, sent_date)
	where id = parm_id;

	select *
	from select_message(parm_id, null,null,null);
end;
$$ LANGUAGE plpgsql