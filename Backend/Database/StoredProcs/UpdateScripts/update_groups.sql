CREATE OR REPLACE FUNCTION update_group(parm_id int, parm_name text, parm_is_private boolean, parm_end_date double precision)
returns TABLE(id int, name text, is_private boolean, end_date double precision) as $$
begin
	UPDATE groups
	set name = coalesce(parm_name, name),
	    is_private = coalesce(parm_is_private, is_private),
	    end_date = coalesce(timestamp without time zone 'epoch' + parm_end_date * interval '1 second', end_date)
	where id = parm_id;

	select *
	from select_group(parm_id, null,null,null);
end;
$$ LANGUAGE plpgsql