CREATE OR REPLACE FUNCTION update_user_group(parm_id int, parm_group int, parm_user int, parm_is_admin char(1))
returns void as $$
begin
	UPDATE users_groups
	set group_id = coalesce(parm_group, group_id),
	    user_id = coalesce(parm_user, user_id),
	    is_admin = coalesce(parm_is_admin, is_admin)
	where id = parm_id;
end;
$$ LANGUAGE plpgsql