﻿CREATE OR REPLACE FUNCTION select_message(parm_id int, parm_user_group_id int, parm_content text, parm_sent_date double precision)
returns TABLE(id int, user_group_id int, content text, sent_date double precision) as $$

	SELECT id, user_group_id, content, EXTRACT(epoch from sent_date) as sent_date
	FROM messages
	WHERE (parm_id is NULL or id = parm_id)
	and (parm_user_group_id is null or user_group_id = parm_user_group_id)
	and (parm_content is null or content = parm_content)
	and (parm_sent_date is null or EXTRACT(epoch from sent_date) = parm_sent_date);
	
$$ LANGUAGE SQL;