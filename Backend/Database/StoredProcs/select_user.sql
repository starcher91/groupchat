CREATE OR REPLACE FUNCTION select_user(parm_id int, parm_email text, parm_password text, parm_name text)
returns TABLE(id int, name text, email text) as $$

	SELECT id, name, email
	FROM users
	WHERE (parm_id is NULL or id = parm_id)
	and (parm_email is null or email = parm_email)
	and (parm_name is null or name = parm_name);
	
$$ LANGUAGE SQL;