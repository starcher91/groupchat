﻿CREATE OR REPLACE FUNCTION select_user_group(parm_id int, parm_group int, parm_user int, parm_is_admin char(1))
returns SETOF users_groups as $$

	SELECT id, user_id, group_id, is_admin
	FROM users_groups
	WHERE (parm_id is NULL or id = parm_id)
	and (parm_group is null or group_id = parm_group)
	and (parm_is_admin is null or is_admin = parm_is_admin);

$$ LANGUAGE SQL;