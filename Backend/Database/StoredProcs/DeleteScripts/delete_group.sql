CREATE OR REPLACE FUNCTION delete_group(parm_id int)
returns void as $$
begin
	DELETE FROM groups
	where id = parm_id;
end;
$$ LANGUAGE plpgsql