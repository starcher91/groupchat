﻿CREATE OR REPLACE FUNCTION delete_message(parm_id int)
returns void as $$
begin
	DELETE FROM messages
	where id = parm_id;
end;
$$ LANGUAGE plpgsql