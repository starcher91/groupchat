CREATE OR REPLACE FUNCTION delete_user(parm_id int)
returns void as $$
begin
	DELETE FROM users
	where id = parm_id;
end;
$$ LANGUAGE plpgsql