CREATE OR REPLACE FUNCTION delete_user_group(parm_id int)
returns void as $$
begin
	DELETE FROM users_groups
	where id = parm_id;
end;
$$ LANGUAGE plpgsql