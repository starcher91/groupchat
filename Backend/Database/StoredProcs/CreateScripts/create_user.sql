CREATE OR REPLACE FUNCTION create_user(email text, password text, name text)
returns void as $$
begin
	INSERT INTO users(email, password, name)
	VALUES (email, password, name);
end;
$$ LANGUAGE plpgsql

