CREATE OR REPLACE FUNCTION create_user_group(parm_group int, parm_user int, parm_is_admin char(1))
returns void as $$
begin
	INSERT INTO users_groups(user_id, group_id, is_admin)
	VALUES (parm_user, parm_group, parm_is_admin);
end;
$$ LANGUAGE plpgsql