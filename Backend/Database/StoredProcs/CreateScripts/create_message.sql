﻿CREATE OR REPLACE FUNCTION create_message(user_group_id int, content text, parm_sent_date double precision)
returns table(id integer, user_group_id int, content text, sent_date double precision) as $$
	INSERT INTO messages(user_group_id, content, sent_date)
	VALUES (user_group_id, content, timestamp without time zone 'epoch' + parm_sent_date * interval '1 second');

	select *
	from select_message(NULL, user_group_id, content, parm_sent_date);
$$ LANGUAGE SQL