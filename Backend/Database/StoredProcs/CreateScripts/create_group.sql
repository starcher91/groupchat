CREATE OR REPLACE FUNCTION create_group(name text, is_private boolean, end_date double precision)
returns table(id integer, name text, is_private boolean, end_date double precision) as $$
	INSERT INTO groups(name, is_private, end_date)
	VALUES (name, is_private, timestamp without time zone 'epoch' + end_date * interval '1 second');

	select *
	from select_group(NULL, name, is_private, null);
$$ LANGUAGE SQL